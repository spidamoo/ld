class Octopus extends Character {
    constructor(x, y) {
        super(x, y);
        this.spritesheet_name = 'octopus';

        this.radius = 0.25;

        this.control = {
            'left':  false,
            'right': false,
            'up':    false,
            'down':  false,
        };

        this.tentacles = 8;
        this.tentacle_is_broken     = [];
        this.tentacles_to_break     = [];
        this.tentacle_directions    = [];
        this.tentacle_fuel          = [];
        this.tentacle_stream_timer  = [];
        for (let i = 0; i < this.tentacles; i++) {
            this.tentacle_is_broken[i]      = false;
            this.tentacle_directions[i]     = undefined;
            this.tentacle_fuel[i]           = 3;
            this.tentacle_stream_timer[i]   = 0;
        }
        this.tentacle_stream_interval = 0.01;
        this.tentacle_trunk_length = 1;

        this.tentacle_joints = [];
        this.tentacle_bodies = [];
        this.tentacle_length         = 1;
        this.tentacle_segments       = 10;
        this.tentacle_segment_length = this.tentacle_length / this.tentacle_segments;
        this.tentacle_width          = 0.1;

        this.tentacle_meshes = [];
        this.tentacle_points = [];
        this.tentacle_broken_meshes = [];
        this.tentacle_broken_points = [];
        this.tentacle_trunk_meshes = [];
        this.tentacle_trunk_points = [];

        this.tentacle_base_local_pos = new b2Vec2(-this.tentacle_segment_length * 0.5, 0);
        this.tentacle_tip_local_pos  = new b2Vec2(+this.tentacle_segment_length * 0.5, 0);

        this.shooting_force = 1;
    }

    init() {
        super.init();

        const circle_shape = new b2CircleShape();
        circle_shape.set_m_radius(this.radius);
        const fixture_def = new b2FixtureDef();
        fixture_def.set_density(2.0);
        fixture_def.set_friction(0.6);
        fixture_def.set_restitution(0.5);
        fixture_def.set_shape(circle_shape);

        const bd = new B2D.b2BodyDef();
        bd.set_type(B2D.b2_dynamicBody);
        bd.set_position( new b2Vec2(this.x / world_scale, this.y / world_scale) );
        bd.set_linearDamping(5);
        bd.set_angularDamping(1);
        bd.set_allowSleep(false);
        this.body = world.CreateBody(bd);
        this.body.octopus = true;

        this.body.CreateFixture( fixture_def );

        for (let i = 0; i < this.tentacles; i++) {
            this.add_tentacle(i);
        }
    }

    add_tentacle(i) {
        const o_x = this.x / world_scale;
        const o_y = this.y / world_scale;

        this.tentacle_bodies[i] = [];

        let prev_body = this.body;
        for (let j = 0; j < this.tentacle_segments; j++) {
            const body = this.add_tentacle_segment(i, j);

            const angle = Math.PI * 2 * i / this.tentacles;
            const joint_x = o_x + Math.cos(angle) * (this.radius + this.tentacle_segment_length * j);
            const joint_y = o_y + Math.sin(angle) * (this.radius + this.tentacle_segment_length * j);

            const joint_def = new b2RevoluteJointDef();
            joint_def.Initialize( prev_body, body, new b2Vec2(joint_x, joint_y) );
            const joint = castObject( world.CreateJoint( joint_def ), b2WheelJoint );

            if (j == this.tentacle_trunk_length) {
                this.tentacle_joints[i] = joint;
            }

            prev_body = body;
        }
    }
    add_tentacle_segment(i, j) {
        const o_x = this.x / world_scale;
        const o_y = this.y / world_scale;

        var shape = new b2PolygonShape();
        shape.SetAsBox(
            this.tentacle_segment_length * 0.5,
            this.tentacle_width * 0.5 * (this.tentacle_segments - j - 1) / this.tentacle_segments + 0.01
        );

        const fixture_def = new b2FixtureDef();
        fixture_def.set_shape(shape);
        fixture_def.set_density( 1.0 );
        fixture_def.set_friction( 0.6 );
        fixture_def.set_restitution( 0.5 );

        const bd = new B2D.b2BodyDef();
        bd.set_type(B2D.b2_dynamicBody);
        this.tentacle_bodies[i][j] = world.CreateBody(bd);
        this.tentacle_bodies[i][j].CreateFixture(fixture_def);
        this.tentacle_bodies[i][j].tentacle = [i, j];

        const angle = Math.PI * 2 * i / this.tentacles;
        const middle_x = o_x + Math.cos(angle) * (this.radius + j * this.tentacle_segment_length + 0.5 * this.tentacle_segment_length);
        const middle_y = o_y + Math.sin(angle) * (this.radius + j * this.tentacle_segment_length + 0.5 * this.tentacle_segment_length);
        this.tentacle_bodies[i][j].SetTransform(new b2Vec2(middle_x, middle_y), angle);

        const bpos = this.tentacle_bodies[i][j].GetPosition();
        // console.log(bpos.get_x(), bpos.get_y());

        return this.tentacle_bodies[i][j];
    }

    draw() {
        const distance_between_points = 90 / this.tentacle_segments;
        for (let i in this.tentacle_bodies) {
            this.tentacle_points[i] = [new PIXI.Point(0, 0)];
            this.tentacle_trunk_points[i] = [new PIXI.Point(0, 0)];
            this.tentacle_broken_points[i] = [];
            for (let j in this.tentacle_bodies[i]) {
                this.tentacle_points[i].push( new PIXI.Point(distance_between_points * j + 5, 0) );
                if (j <= this.tentacle_trunk_length) {
                    this.tentacle_trunk_points[i].push( new PIXI.Point(distance_between_points * j + 5, 0) );
                }
                if (j >= this.tentacle_trunk_length) {
                    this.tentacle_broken_points[i].push( new PIXI.Point(distance_between_points * j + 5, 0) );
                }
            }
            this.tentacle_points[i].push( new PIXI.Point(100, 0) );
            this.tentacle_broken_points[i].push( new PIXI.Point(100, 0) );

            this.tentacle_meshes[i] = new PIXI.mesh.Rope(PIXI.loader.resources.tentacle.texture, this.tentacle_points[i]);
            this.tentacle_meshes[i].visible = false;
            character_layer.addChild(this.tentacle_meshes[i]);

            this.tentacle_trunk_meshes[i] = new PIXI.mesh.Rope(PIXI.loader.resources.tentacle_trunk.texture, this.tentacle_trunk_points[i]);
            this.tentacle_trunk_meshes[i].visible = false;
            character_layer.addChild(this.tentacle_trunk_meshes[i]);

            this.tentacle_broken_meshes[i] = new PIXI.mesh.Rope(PIXI.loader.resources.tentacle_broken.texture, this.tentacle_broken_points[i]);
            this.tentacle_broken_meshes[i].visible = false;
            character_layer.addChild(this.tentacle_broken_meshes[i]);
        }

        super.draw();
        // console.log(this.tentacle_points, this.tentacle_meshes);
    }

    handle_keydown(event) {
        if (this.dead) {
            return;
        }

        switch (event.code) {
            case 'ArrowLeft':
            case 'KeyA':
                this.control.left = true;
            break;
            case 'ArrowRight':
            case 'KeyD':
                this.control.right = true;
            break;
            case 'ArrowUp':
            case 'KeyW':
                this.control.up = true;
            break;
            case 'ArrowDown':
            case 'KeyS':
                this.control.down = true;
            break;
            case 'KeyT':
                if (DEBUG) {
                    this.x = this.o_x = this.target_x;
                    this.y = this.o_y = this.target_y;
                }
            break;
            case 'KeyI':
                if (DEBUG) {
                    this.invincible = true;
                }
            break;
        }
        if (event.code.substr(0, 5) == 'Digit') {
            const digit = parseInt( event.code.substr(5) );
            if (digit >= 1 && digit <= 8) {
                this.break_tentacle(digit - 1);
            }
        }
    }
    handle_keyup(event) {
        if (this.dead) {
            return;
        }
        switch (event.code) {
            case 'ArrowLeft':
            case 'KeyA':
                this.control.left = false;
            break;
            case 'ArrowRight':
            case 'KeyD':
                this.control.right = false;
            break;
            case 'ArrowUp':
            case 'KeyW':
                this.control.up = false;
            break;
            case 'ArrowDown':
            case 'KeyS':
                this.control.down = false;
            break;
        }
    }
    handle_click(event) {
        if (this.dead) {
            return;
        }

        this.target_x = -objects_on_map_layer.x + event.layerX;
        this.target_y = -objects_on_map_layer.y + event.layerY;

        // console.log(this.x, this.y, this.target_x, this.target_y, event);
        const dx = this.target_x - this.x;
        const dy = this.target_y - this.y;

        this.target_angle = Math.atan2(dy, dx);

        let closest_tentacle;
        let closest_distance;
        for (let i = 0; i < this.tentacles; i++) {
            if (this.tentacle_is_broken[i]) {
                continue;
            }

            const tip_body = this.tentacle_bodies[i][this.tentacle_segments - 1];
            const bpos = tip_body.GetPosition();
            const distance_to_tip = distance_between(
                bpos.get_x() * world_scale, bpos.get_y() * world_scale,
                this.target_x, this.target_y
            );

            // console.log(i, diff, closest_tentacle, angle_diff);
            if (closest_tentacle === undefined || distance_to_tip < closest_distance) {
                closest_tentacle = i;
                closest_distance = distance_to_tip;
            }
        }

        // console.log(closest_tentacle);

        if (closest_tentacle !== undefined) {
            this.break_tentacle(closest_tentacle, this.target_angle);
        }
        // console.log(this.x, this.y, this.target_x, this.target_y);
        // this.firing = true;
    }
    handle_unclick(event) {
        if (this.dead) {
            return;
        }
    }
    handle_mouse(event) {
        return;
        if (this.dead) {
            return;
        }
        this.target_x = -character_layer.x + event.pageX;
        this.target_y = -character_layer.y + event.pageY;
    }

    update(dt) {
        if (this.control.right) {
            if (this.control.down) {
                this.move_direction = Math.PI * 0.25;
            }
            else if (this.control.up) {
                this.move_direction = -Math.PI * 0.25;
            }
            else {
                this.move_direction = 0;
            }
        }
        else if (this.control.left) {
            if (this.control.down) {
                this.move_direction =  Math.PI * 0.75;
            }
            else if (this.control.up) {
                this.move_direction = -Math.PI * 0.75;
            }
            else {
                this.move_direction = -Math.PI;
            }
        }
        else {
            if (this.control.down) {
                this.move_direction =  Math.PI * 0.5;
            }
            else if (this.control.up) {
                this.move_direction = -Math.PI * 0.5;
            }
            else {
                this.move_direction = undefined;
            }
        }

        super.update(dt);

        for (let i in this.tentacle_bodies) {
            if (this.tentacle_is_broken[i]) {
                this.tentacle_trunk_points[i][0].x = this.x;
                this.tentacle_trunk_points[i][0].y = this.y;
                for (let j in this.tentacle_bodies[i]) {
                    j = Number(j);
                    const bpos = this.tentacle_bodies[i][j].GetWorldPoint(this.tentacle_base_local_pos);
                    if (j < this.tentacle_trunk_length) {
                        this.tentacle_trunk_points[i][j + 1].x = bpos.get_x() * world_scale;
                        this.tentacle_trunk_points[i][j + 1].y = bpos.get_y() * world_scale;
                    }
                    if (j == this.tentacle_trunk_length - 1) {
                        const tip_pos = this.tentacle_bodies[i][j].GetWorldPoint(this.tentacle_tip_local_pos);
                        this.tentacle_trunk_points[i][j + 2].x = tip_pos.get_x() * world_scale;
                        this.tentacle_trunk_points[i][j + 2].y = tip_pos.get_y() * world_scale;
                    }
                    if (j >= this.tentacle_trunk_length) {
                        this.tentacle_broken_points[i][j - this.tentacle_trunk_length].x = bpos.get_x() * world_scale;
                        this.tentacle_broken_points[i][j - this.tentacle_trunk_length].y = bpos.get_y() * world_scale;
                    }
                }
                const tip_pos = this.tentacle_bodies[i][this.tentacle_segments - 1].GetWorldPoint(this.tentacle_tip_local_pos);
                // console.log(tip_pos.get_x(), tip_pos.get_y());
                this.tentacle_broken_points[i][this.tentacle_segments - this.tentacle_trunk_length].x = tip_pos.get_x() * world_scale;
                this.tentacle_broken_points[i][this.tentacle_segments - this.tentacle_trunk_length].y = tip_pos.get_y() * world_scale;
            }
            else {
                this.tentacle_points[i][0].x = this.x;
                this.tentacle_points[i][0].y = this.y;
                for (let j in this.tentacle_bodies[i]) {
                    j = Number(j);
                    const bpos = this.tentacle_bodies[i][j].GetWorldPoint(this.tentacle_base_local_pos);
                    // console.log(this.tentacle_points, i, j + 1, 1 + j);
                    this.tentacle_points[i][j + 1].x = bpos.get_x() * world_scale;
                    this.tentacle_points[i][j + 1].y = bpos.get_y() * world_scale;
                }
                const tip_pos = this.tentacle_bodies[i][this.tentacle_segments - 1].GetWorldPoint(this.tentacle_tip_local_pos);
                // console.log(tip_pos.get_x(), tip_pos.get_y());
                this.tentacle_points[i][this.tentacle_segments + 1].x = tip_pos.get_x() * world_scale;
                this.tentacle_points[i][this.tentacle_segments + 1].y = tip_pos.get_y() * world_scale;
            }
        }

        while (this.tentacles_to_break.length) {
            const i = this.tentacles_to_break.shift();
            this.break_tentacle(i);
        }

        let active_hint;
        for (let hint of map_hints) {
            if (this.x > hint.ox && this.x < hint.ex && this.y > hint.oy && this.y < hint.ey) {
                active_hint = hint;
            }
        }

        if (active_hint) {
            hint_text.text = active_hint.text;
            hint_text.visible = true;
        }
        else {
            hint_text.visible = false;
        }

        for (let out of outs) {
            if (this.x > out.ox && this.x < out.ex && this.y > out.oy && this.y < out.ey) {
                if (out.to == '<win>') {
                    this.won = true;
                }
                else {
                    finish_level();
                    start_level(out.to);
                }
            }
        }
    }

    update_physics(dt) {
        super.update_physics(dt);

        for (let i = 0; i < this.tentacles; i++) {
            if (!this.tentacle_is_broken[i] || this.tentacle_directions[i] === undefined || this.tentacle_fuel[i] < 0) {
                continue;
            }
            let dx = Math.cos(this.tentacle_directions[i]) * this.shooting_force;
            let dy = Math.sin(this.tentacle_directions[i]) * this.shooting_force;
            this.force_vector.Set(dx, dy);
            const tip_body = this.tentacle_bodies[i][this.tentacle_segments - 1];
            tip_body.ApplyForce( this.force_vector, tip_body.GetWorldPoint(ZERO_VECTOR) );

            this.tentacle_fuel[i] -= dt;
            this.tentacle_stream_timer[i] -= dt;

            while (this.tentacle_stream_timer[i] < 0) {
                this.tentacle_stream_timer[i] += this.tentacle_stream_interval;
                const base_body = this.tentacle_bodies[i][this.tentacle_trunk_length];
                const bpos = base_body.GetPosition();
                add_particle('inkspot', bpos.get_x() * world_scale, bpos.get_y() * world_scale);
            }
            // console.log('move', dx, dy);
        }
    }

    put_at(x, y) {
        super.put_at(x, y);

        const o_x = this.x / world_scale;
        const o_y = this.y / world_scale;

        // this.body.SetTransform( new b2Vec2(x / world_scale, y / world_scale), 0 );
        for (let i in this.tentacle_bodies) {
            const angle = Math.PI * 2 * i / this.tentacles;
            for (let j in this.tentacle_bodies[i]) {
                const middle_x = o_x + Math.cos(angle) * (this.radius + j * this.tentacle_segment_length + 0.5 * this.tentacle_segment_length);
                const middle_y = o_y + Math.sin(angle) * (this.radius + j * this.tentacle_segment_length + 0.5 * this.tentacle_segment_length);

                this.tentacle_bodies[i][j].SetTransform( new b2Vec2(middle_x, middle_y), angle );
            }
        }
    }

    show() {
        super.show();
        for (let i in this.tentacle_meshes) {
            if (this.tentacle_is_broken[i]) {
                this.tentacle_trunk_meshes[i].visible = true;
                this.tentacle_broken_meshes[i].visible = true;
            }
            else {
                this.tentacle_meshes[i].visible = true;
            }
        }
    }

    break_tentacle(i, shoot_direction) {
        if (this.tentacle_is_broken[i]) {
            return;
        }

        this.tentacle_is_broken[i] = true;
        this.tentacle_trunk_meshes[i].visible = true;
        this.tentacle_broken_meshes[i].visible = true;
        this.tentacle_meshes[i].visible = false;

        if (shoot_direction !== undefined) {
            this.tentacle_directions[i] = shoot_direction;
            PIXI.loader.resources.shoot.sound.play();
        }

        world.DestroyJoint(this.tentacle_joints[i]);
    }
    promise_to_break_tentacle(i) {
        this.tentacles_to_break.push(i);
    }
    hit_by_enemy(enemy_body) {
        if (enemy_body.enemy.dead) {
            return;
        }
        // console.log('hit_by_enemy', enemy_body.enemy);
        this.kill();
    }

    is_attractive() {
        return !this.dead && !this.won;
    }

    kill() {
        if (this.dead || this.invincible) {
            return;
        }
        super.kill();
        this.move_direction = undefined;
        this.control = {};
        this.set_anim('dead');
    }
}

class Altar extends Character {
    constructor(x, y) {
        super(x, y);

        this.spritesheet_name = 'altar';
        this.anims = ['idle', 'glow'];

        this.detached_from_body = true;

        this.glow_distance = 100;
    }

    draw() {
        super.draw();
        this.set_anim('idle');
    }

    update(dt) {
        super.update(dt);

        if (this.current_anim == 'idle' && distance_between(this.x, this.y, player.x, player.y) < this.glow_distance) {
            this.set_anim('glow');
        }
        else if (this.current_anim == 'glow' && distance_between(this.x, this.y, player.x, player.y) > this.glow_distance) {
            this.set_anim('idle');
        }
    }
}
