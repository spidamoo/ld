class Character {
    constructor(x, y) {
        const self = this;

        this.x = x || 0;
        this.y = y || 0;
        this.target_x = this.x;
        this.target_y = this.y;
        this.rotation = 0;

        this.dead = false;

        this.anims = ['move', 'attack', 'dead'];
        this.anim_speeds = {
            'default': 0.1,
        };
        this.anim_transitions = {'attack': 'stop', 'dead': 'stop'};

        this.force_vector = new b2Vec2(0, 0);
        this.moving_force = 10;
        this.stopping_force = 2;
    }

    init() {
    }
    draw() {
        this.graphics = new PIXI.Container();
        this.graphics.visible = false;

        this.spritesheet = PIXI.loader.resources[this.spritesheet_name].spritesheet;
        this.animations = [];
        for (let anim of this.anims) {
            if (!this.spritesheet.animations[anim]) {
                continue;
            }

            this.animations[anim] = new PIXI.extras.AnimatedSprite(
                this.spritesheet.animations[anim]
            );
            this.animations[anim].anchor.x = 0.5;
            this.animations[anim].anchor.y = 0.5;
            this.animations[anim].visible = false;
            this.animations[anim].animationSpeed = this.anim_speeds[anim] || this.anim_speeds.default;
            this.graphics.addChild(this.animations[anim]);
        }

        const self = this;
        for (let anim in this.anim_transitions) {
            if (!this.animations[anim]) {
                continue;
            }
            this.animations[anim].loop = false;
            if (self.anim_transitions[anim] != 'stop') {
                this.animations[anim].onComplete = function() {
                    self.set_anim(self.anim_transitions[anim]);
                }
            }
        }

        this.set_anim('move');

        character_layer.addChild(this.graphics);
    }

    set_anim(anim) {
        // console.log(this.current_anim, anim);
        if (!this.animations[anim]) {
            return;
        }
        if (this.current_anim) {
            this.animations[this.current_anim].visible = false;
            this.animations[this.current_anim].stop();
        }
        this.current_anim = anim;
        this.animations[this.current_anim].visible = true;
        this.animations[this.current_anim].gotoAndPlay(0);
        // console.log(this.current_anim);
    }

    update(dt) {
        this.update_physics(dt);

        this.update_anims();

        this.graphics.x = this.x;
        this.graphics.y = this.y;
        this.graphics.rotation = this.rotation;
    }

    update_physics(dt) {
        if (this.detached_from_body) {
            return;
        }

        if (this.move_direction !== undefined) {
            // console.log(this.moving_force);
            let dx = Math.cos(this.move_direction) * this.moving_force;
            let dy = Math.sin(this.move_direction) * this.moving_force;
            this.force_vector.Set(dx, dy);
            this.body.ApplyForce( this.force_vector, this.body.GetWorldPoint(ZERO_VECTOR) );
        }

        const bpos = this.body.GetPosition();
        this.x = bpos.get_x() * world_scale;
        this.y = bpos.get_y() * world_scale;
        this.rotation = this.body.GetAngle();
    }

    update_anims() {}

    sees(who) {
        return true;
    }
    kill(angle) {
        if (this.dead || this.invincible) {
            return;
        }
        this.dead = true;
    }
    put_at(x, y) {
        this.x = x;
        this.y = y;
        if (!this.detached_from_body) {
            this.body.SetTransform( new b2Vec2(x / world_scale, y / world_scale), 0 );
        }

        this.show();
    }
    show() {
        this.graphics.visible = true;
    }
    hide() {
        this.graphics.visible = false;
    }

    destroy() {}
}


