

let dd_container;
let color_num;

const line_alpha = 0.4;
const fill_alpha = 0.2;

function clearDebugDraw() {
    // console.log('clearDebugDraw');
    dd_container.removeChildren();
}

function getDebugDrawDisplayObject() {
    dd_container = new PIXI.Container();
    return dd_container;
}

function drawAxes(graphics) {
    graphics.moveTo(0, 0);
    graphics.lineTo(world_scale, 0);
    graphics.moveTo(0, 0);
    graphics.lineTo(0, world_scale);
    return graphics;

    // ctx.strokeStyle = 'rgb(192,0,0)';
    // ctx.beginPath();
    // ctx.moveTo(0, 0);
    // ctx.lineTo(1, 0);
    // ctx.stroke();
    // ctx.strokeStyle = 'rgb(0,192,0)';
    // ctx.beginPath();
    // ctx.moveTo(0, 0);
    // ctx.lineTo(0, 1);
    // ctx.stroke();
}

function setColorFromDebugDrawCallback(graphics, color) {
    const col = B2D.wrapPointer(color, B2D.b2Color);
    const red = (col.get_r() * 255) | 0;
    const green = (col.get_g() * 255) | 0;
    const blue = (col.get_b() * 255) | 0;
    // const colStr = red+","+green+","+blue;
    color_num = (red << 4) + (green << 2) + blue;

    graphics.lineStyle(1, color_num, line_alpha);
}

function drawSegment(graphics, vert1, vert2) {
    var vert1V = B2D.wrapPointer(vert1, B2D.b2Vec2);
    var vert2V = B2D.wrapPointer(vert2, B2D.b2Vec2);

    graphics.moveTo(vert1V.get_x() * world_scale, vert1V.get_y() * world_scale);
    graphics.lineTo(vert2V.get_x() * world_scale, vert2V.get_y() * world_scale);

    // context.beginPath();
    // context.moveTo(vert1V.get_x(),vert1V.get_y());
    // context.lineTo(vert2V.get_x(),vert2V.get_y());
    // context.stroke();
}

function drawPolygon(graphics, vertices, vertexCount, fill) {
    if (fill) {
        graphics.beginFill(color_num, fill_alpha);
    }

    const path = [];
    for (tmpI=0; tmpI < vertexCount; tmpI++) {
        let vert = B2D.wrapPointer(vertices + (tmpI * 8), B2D.b2Vec2);
        path.push( vert.get_x() * world_scale, vert.get_y() * world_scale );
    }

    graphics.drawPolygon(path);
    graphics.closePath();

    if (fill) {
        graphics.endFill();
    }

    // context.beginPath();
    // for(tmpI=0;tmpI<vertexCount;tmpI++) {
    //     var vert = B2D.wrapPointer(vertices+(tmpI*8), B2D.b2Vec2);
    //     if ( tmpI == 0 )
    //         context.moveTo(vert.get_x(),vert.get_y());
    //     else
    //         context.lineTo(vert.get_x(),vert.get_y());
    // }
    // context.closePath();
    // if (fill)
    //     context.fill();
    // context.stroke();
}

function drawCircle(graphics, center, radius, axis, fill) {
    if (fill) {
        graphics.beginFill(color_num, fill_alpha);
    }

    let centerV = B2D.wrapPointer(center, B2D.b2Vec2);

    graphics.drawCircle(centerV.get_x() * world_scale,centerV.get_y() * world_scale, radius * world_scale);

    if (fill) {
        graphics.endFill();
    }

    // context.beginPath();
    // context.arc(centerV.get_x(),centerV.get_y(), radius, 0, 2 * Math.PI, false);
    // if (fill)
    //     context.fill();
    // context.stroke();

    if (fill) {
        let axisV = B2D.wrapPointer(axis, B2D.b2Vec2);
        //render axis marker
        var vert2V = copyVec2(centerV);
        vert2V.op_add( scaledVec2(axisV, radius) );

        graphics.moveTo( centerV.get_x() * world_scale, centerV.get_y() * world_scale );
        graphics.lineTo(  vert2V.get_x() * world_scale,  vert2V.get_y() * world_scale );
        // context.beginPath();
        // context.moveTo(centerV.get_x(),centerV.get_y());
        // context.lineTo(vert2V.get_x(),vert2V.get_y());
        // context.stroke();
    }
}

function drawTransform(graphics, transform) {
    var trans = B2D.wrapPointer(transform,B2D.b2Transform);
    var pos = trans.get_p();
    var rot = trans.get_q();

    graphics.x = pos.get_x() * world_scale;
    graphics.y = pos.get_y() * world_scale;
    graphics.scale.x = 0.5;
    graphics.scale.y = 0.5;
    graphics.rotation = rot.GetAngle();
    graphics.lineStyle(2, 0xC00000, line_alpha);
    drawAxes(graphics);

    // context.save();
    // context.translate(pos.get_x(), pos.get_y());
    // context.scale(0.5,0.5);
    // context.rotate(rot.GetAngle());
    // context.lineWidth *= 2;
    // drawAxes(context);
    // context.restore();
}

function getDebugDraw() {
    const debugDraw = new B2D.JSDraw();

    debugDraw.DrawSegment = function(vert1, vert2, color) {
        // console.log('DrawSegment');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        setColorFromDebugDrawCallback(graphics, color);
        drawSegment(graphics, vert1, vert2);
    };

    debugDraw.DrawPolygon = function(vertices, vertexCount, color) {
        // console.log('DrawPolygon');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        setColorFromDebugDrawCallback(graphics, color);
        drawPolygon(graphics, vertices, vertexCount, false);
    };

    debugDraw.DrawSolidPolygon = function(vertices, vertexCount, color) {
        // console.log('DrawSolidPolygon');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        setColorFromDebugDrawCallback(graphics, color);
        drawPolygon(graphics, vertices, vertexCount, true);
    };

    debugDraw.DrawCircle = function(center, radius, color) {
        // console.log('DrawCircle');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        setColorFromDebugDrawCallback(graphics, color);
        var dummyAxis = B2D.b2Vec2(0,0);
        drawCircle(graphics, center, radius, dummyAxis, false);
    };

    debugDraw.DrawSolidCircle = function(center, radius, axis, color) {
        // console.log('DrawSolidCircle');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        setColorFromDebugDrawCallback(graphics, color);
        drawCircle(graphics, center, radius, axis, true);
    };

    debugDraw.DrawTransform = function(transform) {
        // console.log('DrawTransform');
        const graphics = new PIXI.Graphics();
        dd_container.addChild(graphics);
        drawTransform(graphics, transform);
    };

    debugDraw.SetFlags(0x1);

    return debugDraw;
}