class Enemy extends Character {
    constructor(type, x, y) {
        super(x, y);
        this.type = type;
        this.spritesheet_name = type;

        this.resistance   = 5;

        this.state          = 'idle';
        this.state_timer    = 0;
        this.state_phase    = 0;

        this.anim_speeds = {
            'default': 0.2,
        };

        this.hw = 0.4;
        this.hh = 0.2;

        this.aggro_distance  = 400;
        this.attack_distance = 200;
    }
    init() {
        super.init();

        const shape = new b2PolygonShape();
        shape.SetAsBox(this.hw, this.hh);
        const fixture_def = new b2FixtureDef();
        fixture_def.set_density(2.0);
        fixture_def.set_friction(0.6);
        fixture_def.set_restitution(0.5);
        fixture_def.set_shape(shape);

        const bd = new B2D.b2BodyDef();
        bd.set_type(B2D.b2_dynamicBody);
        bd.set_position( new b2Vec2(this.x / world_scale, this.y / world_scale) );
        bd.set_linearDamping(5);
        bd.set_angularDamping(0.1);
        bd.set_fixedRotation(true);
        bd.set_allowSleep(false);
        this.body = world.CreateBody(bd);
        this.body.CreateFixture(fixture_def);
        this.body.enemy = this;
    }
    draw() {
        super.draw();
    }

    update(dt) {
        super.update(dt);

        this.state_timer += dt;

        if (this.dead && this.state != 'dead') {
            this.set_state('dead');
        }
    }

    update_anims() {
        super.update_anims();

        if (this.current_anim == 'dead' && this.graphics.scale.y == 1) {
            this.graphics.scale.y = -1;
        }
    }

    set_state(state) {
        this.state = state;
        this.state_phase = 0;
        this.state_timer = 0;
    }
    is_attacking() {
        return false;
    }

    hit_with_tentacle(tentacle_body) {
        if (this.dead) {
            PIXI.loader.resources.weak_hit.sound.play();
            return;
        }
        const velocity = tentacle_body.GetLinearVelocity();
        const speed    = velocity.Length();
        if (speed > 2) {
            // console.log( 'hit_with_tentacle', speed );
        }

        const [i, j] = tentacle_body.tentacle;
        if (!player.tentacle_is_broken[i]) {
            if ( this.is_attacking() ) {
                player.promise_to_break_tentacle(i);
            }
            else if (speed > this.resistance) {
                PIXI.loader.resources.strong_hit.sound.play();
                this.hit(speed);
            }
            else {
                PIXI.loader.resources.weak_hit.sound.play();
            }
        }
        else if (speed > this.resistance) {
            this.hit(speed);
            PIXI.loader.resources.strong_hit.sound.play();
        }
        else {
            PIXI.loader.resources.weak_hit.sound.play();
        }
    }
    hit() {
        this.kill();
    }
    kill() {
        super.kill();
    }
}


class Shark extends Enemy {
    constructor(x, y) {
        super('shark', x, y);

        this.normal_force = 4;
        this.hunt_force   = 18;
        this.attack_force = 30;

        this.attack_point = 0.7;
        this.attack_time  = 1.2;

        this.aggro_distance = 400;

        this.idle_phases = [
            {d: 0,          l: 1},
            {d: undefined,  l: 0.3},
            {d: Math.PI,    l: 1},
            {d: undefined,  l: 0.3},
        ];
    }

    update(dt) {
        super.update(dt);

        switch(this.state) {
            case 'idle': {
                if (this.state_timer > this.idle_phases[this.state_phase].l) {
                    this.state_phase++;
                    if (this.state_phase == this.idle_phases.length) {
                        this.state_phase = 0;
                    }
                    this.move_direction = this.idle_phases[this.state_phase].d;
                    this.state_timer = 0;
                }
                this.moving_force = this.normal_force;

                if ( player.is_attractive() && this.sees(player) ) {
                    const distance_to_player = distance_between(this.x, this.y, player.x, player.y);
                    if (distance_to_player < this.aggro_distance) {
                        this.set_state('approaching');
                    }
                }
            } break;
            case 'approaching': {
                if ( !player.is_attractive() || !this.sees(player) ) {
                    this.set_state('idle');
                    break;
                }

                const distance_to_player = distance_between(this.x, this.y, player.x, player.y);
                const direction_to_player = Math.atan2(player.y - this.y, player.x - this.x);
                this.move_direction = direction_to_player;
                this.moving_force = this.hunt_force;

                if (distance_to_player < this.attack_distance) {
                    this.set_state('attack');
                }
                else if (distance_to_player > this.aggro_distance) {
                    this.set_state('idle');
                }
            } break;
            case 'attack': {
                switch (this.state_phase) {
                    case 0:
                        this.set_anim('attack');
                        this.state_phase = 1;
                        this.moving_force = 0;
                        break;
                    case 1:
                        if (this.state_timer > this.attack_point) {
                            const direction_to_player = Math.atan2(player.y - this.y, player.x - this.x);
                            this.move_direction = direction_to_player;
                            this.state_phase = 2;
                            this.moving_force = this.attack_force;
                            PIXI.loader.resources.shark_bite.sound.play();
                        }
                        break;
                    case 2:
                        if (this.state_timer > this.attack_time) {
                            this.set_state('idle');
                            this.set_anim('move');
                        }
                        break;
                }
            } break;
            case 'dead': {
                switch (this.state_phase) {
                    case 0:
                        this.set_anim('dead');
                        this.state_phase = 1;
                        this.moving_force = 0.5;
                        this.move_direction = -Math.PI * 0.5;
                        break;
                }
            } break;
        }
    }

    update_anims() {
        super.update_anims();

        const velocity = this.body.GetLinearVelocity();
        const dx = velocity.get_x();

        if (dx > 0 && this.graphics.scale.x == -1) {
            this.graphics.scale.x = 1;
        }
        else if (dx < 0 && this.graphics.scale.x == 1) {
            this.graphics.scale.x = -1;
        }
    }

    is_attacking() {
        return this.state == 'attack' && this.state_phase == 2;
    }
}

class Medusa extends Enemy {
    constructor(x, y) {
        super('medusa', x, y);

        this.hw = 0.2;
        this.hh = 0.2;

        this.attack_distance = 150;

        this.ascend_force = 10;
        this.descend_force = 2.5;

        this.ascend_frame  = 4;
        this.descend_frame = 6;

        this.attack_time  = 1.0;
        this.attack_point = 0.3;
        this.death_time   = 1.0;
    }

    update(dt) {
        super.update(dt);

        switch(this.state) {
            case 'idle': {
                this.alternate_move_mode(-Math.PI * 0.5, Math.PI * 0.5);

                if ( player.is_attractive() && this.sees(player) ) {
                    const distance_to_player = distance_between(this.x, this.y, player.x, player.y);
                    if (distance_to_player < this.aggro_distance) {
                        this.state = 'approaching';
                    }
                }
            } break;
            case 'approaching': {
                if ( !player.is_attractive() || !this.sees(player) ) {
                    this.state = 'idle';
                    break;
                }

                const distance_to_player = distance_between(this.x, this.y, player.x, player.y);
                const direction_to_player = Math.atan2(player.y - this.y, player.x - this.x);

                this.alternate_move_mode(direction_to_player, direction_to_player);

                if (distance_to_player < this.attack_distance) {
                    this.set_state('attack');
                }
                else if (distance_to_player > this.aggro_distance) {
                    this.state = 'idle';
                }
            } break;
            case 'attack': {
                switch (this.state_phase) {
                    case 0:
                        this.set_anim('attack');
                        this.state_phase = 1;
                        this.move_direction = undefined;
                    case 1:
                        if (this.state_timer > this.attack_point) {
                            PIXI.loader.resources.medusalightning_sound.sound.play();
                        }
                    case 2:
                    case 3:
                        if (this.state_timer > this.attack_point + this.state_phase * 0.1) {
                            this.state_phase++;
                            add_particle('spark', this.x, this.y);
                            player.kill();
                        }
                        break;
                    case 4:
                        if (this.state_timer > this.attack_time) {
                            this.set_state('idle');
                            this.set_anim('move');
                        }
                        break;
                }
            } break;
            case 'dead': {
                switch (this.state_phase) {
                    case 0:
                        this.set_anim('dead');
                        this.state_phase = 1;
                        this.moving_force = 0.5;
                        this.move_direction = -Math.PI * 0.5;
                        world.DestroyBody(this.body);
                        this.detached_from_body = true;
                        break;
                    case 1:
                        if (this.state_timer > this.death_time) {
                            this.graphics.visible = false;
                            this.state_phase = 2;
                        }
                }
            } break;
        }
    }

    alternate_move_mode(ascend_direction, descend_direction) {
        switch (this.state_phase) {
            case 0:
                if (this.animations[this.current_anim].currentFrame == this.ascend_frame) {
                    this.state_timer = 0;
                    this.state_phase = 1;
                    this.moving_force = this.ascend_force;
                    this.move_direction = ascend_direction;
                }
                break;
            case 1:
                if (this.animations[this.current_anim].currentFrame == this.descend_frame) {
                    this.state_timer = 0;
                    this.state_phase = 0;
                    this.moving_force = this.descend_force;
                    this.move_direction = descend_direction;
                }
        }
    }
}

class Fish extends Enemy {
    constructor(type, x, y) {
        super(type, x, y);

        this.hw = 0.2;
        this.hh = 0.1;
        this.normal_force = 1;

        this.resistance   = 2;

        this.idle_phases = [
            {d: 0,          l: 1},
            {d: undefined,  l: 0.3},
            {d: Math.PI,    l: 1},
            {d: undefined,  l: 0.3},
        ];
    }

    update(dt) {
        super.update(dt);

        switch(this.state) {
            case 'idle': {
                if (this.state_timer > this.idle_phases[this.state_phase].l) {
                    this.state_phase++;
                    if (this.state_phase == this.idle_phases.length) {
                        this.state_phase = 0;
                    }
                    this.move_direction = this.idle_phases[this.state_phase].d;
                    this.state_timer = 0;
                }
                this.moving_force = this.normal_force;
            } break;
            case 'dead': {
                switch (this.state_phase) {
                    case 0:
                        this.set_anim('dead');
                        this.state_phase = 1;
                        this.moving_force = 0.5;
                        this.move_direction = -Math.PI * 0.5;
                        break;
                }
            } break;
        }
    }

    update_anims() {
        super.update_anims();

        const velocity = this.body.GetLinearVelocity();
        const dx = velocity.get_x();

        if (dx > 0.1 && this.graphics.scale.x == -1) {
            this.graphics.scale.x = 1;
        }
        else if (dx < -0.1 && this.graphics.scale.x == 1) {
            this.graphics.scale.x = -1;
        }
    }
}

class BrownFish extends Fish {
    constructor(x, y) {
        super('brown_fish', x, y);
    }
}
class YellowFish extends Fish {
    constructor(x, y) {
        super('yellow_fish', x, y);
    }
}

const enemy_classes = {
    'shark': Shark,
    'medusa': Medusa,
    'brown_fish': BrownFish,
    'yellow_fish': YellowFish,
};
