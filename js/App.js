$(function() {
    show_main_menu();
    init_app();

    $('button.show_main_menu').click(function() {
        show_main_menu();
    });
    $('button.escape').click(function() {
        game_over();
        show_main_menu();
    });
    $('button.start_game').click(function() {
        start_game();
    });
    $('button.restart_level').click(function() {
        start_game(last_level);
    });
    $('button.show_credits').click(function() {
        show_credits();
    });
});

let app;
let window_w;
let window_h;
let _init = $.Deferred();
let _init_game;

const search_params = window.location.search.substring(1).split('&').reduce( (obj, p) => {const [n, v] = p.split('='); obj[n] = v === undefined ? true : v; return obj}, {} );
const DEBUG = search_params.debug;
const DEBUG_DRAW = DEBUG;

function init_app() {
    app = new PIXI.Application();
    window_w = $(window).width();
    window_h = $(window).height() - 5;
    app.renderer.autoResize = true;
    app.renderer.resize(window_w, window_h);

    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    PIXI.loader
        .add('octopus', 'img/octopus.json')
        .add('tentacle', 'img/octopus/tenta100E.png')
        .add('tentacle_trunk', 'img/octopus/tentapartE.png')
        .add('tentacle_broken', 'img/octopus/tentabullitE.png')
        .add('altar', 'img/altar.json')
        .add('shark', 'img/enemies/shark.json')
        .add('medusa', 'img/enemies/medusa.json')
        .add('brown_fish', 'img/neutrals/brown_fish.json')
        .add('yellow_fish', 'img/neutrals/yellow_fish.json')
        .add('light', 'img/background/light.json')
        .add('bubbles', 'img/background/bubles.json')
        .add('medusalightning', 'img/enemies/medusalightning.png')
        .add('inkspot', 'img/octopus/inkspot.png')
        .add('inkspot2', 'img/octopus/inkspot2.png')
        
        // .add('game_over', 'sound/game_over.wav')
        .add('game_over', 'sound/game_over_3.mp3')
        .add('win', 'sound/win3.wav')
        .add('enter_level', 'sound/move1.wav')
        .add('medusalightning_sound', 'sound/lighting_start.wav')
        .add('weak_hit', 'sound/hit12.wav')
        .add('strong_hit', 'sound/hit12.wav')
        .add('shoot', 'sound/splash1.wav')
        .add('shark_bite', 'sound/shark-bite.wav')

        .add('music', 'sound/music1.mp3')
    ;

    app.stage = new PIXI.display.Stage();
    // app.stage.group.enableSort = true;

    app.ticker.add( () => {
        const dt = 0.017;//app.ticker.elapsedMS * 0.001;
        update_game(dt);
    } );
    app.ticker.stop();

    PIXI.loader.load( (loader, resources) => {
        const sound_volumes = {
            enter_level  : 0.7,
            game_over    : 0.7,
            strong_hit: 0.5,
            weak_hit     : 0.05,
            medusalightning_sound: 0.1,
            shark_bite: 0.2,
        };
        for (let sound in sound_volumes) {
            loader.resources[sound].sound.volume = sound_volumes[sound];
        }

        loader.resources.music.sound.loop = true;
        _init.resolve();
    } );
}

function show_main_menu() {
    $('.screen').hide();
    $('#main_menu').show();
}

function show_game_over() {
    $('.screen').hide();
    $('#game_over').show();
}

function start_game(level) {
    $('.screen').hide();

    _init.done(function() {
        _init_game = init_game();
        PIXI.loader.resources.music.sound.volume = 1;
        PIXI.loader.resources.music.sound.play();
        _init_game.done(function() {
            document.onkeydown      = event => player.handle_keydown(event);
            document.onkeyup        = event => player.handle_keyup(event);
            app.view.onmousedown    = event => player.handle_click(event);
            app.view.onmouseup      = event => player.handle_unclick(event);
            app.view.onmousemove    = event => player.handle_mouse(event);

            app.ticker.start();

            level = level || search_params.level || 'level0';
            start_level(level);

            $('#renderer').append(app.view);
            $('#game').show();
        });
    });
}

function finish_game() {
    finish_level();
    app.ticker.stop();
    console.log('stop');
    app.stage.removeChildren();

    document.onkeydown      = undefined;
    document.onkeyup        = undefined;

    PIXI.loader.resources.music.sound.stop();
    $('#renderer').empty();
}

function game_over(win) {
    finish_game();
    if (win) {
        show_win_screen();
    }
    else {
        show_game_over();
    }
}

function show_win_screen() {
    $('.screen').hide();
    $('#win').show();
}

function show_credits() {
    $('.screen').hide();
    $('#credits').show();
}
