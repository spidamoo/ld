

function distance_between(x1, y1, x2, y2) {
    return Math.sqrt( (y1 - y2) * (y1 - y2) + (x1 - x2) * (x1 - x2) );
}


const level_loaders = {};
const level_xmls    = {};
const map_textures      = [];
const map_tiles         = [];
const map_animations    = [];
const map_layers        = [];
const map_objects       = [];
const map_enemies       = [];
const map_hints         = [];
let   level_width;
let   level_height;
let   tile_extra_height = 0;
let   tile_extra_width  = 0;
let   map_tile_width  = 16;
let   map_tile_height = 16;

const ins  = [];
const outs = [];
let   map_body;

function load_level(level_name) {
    current_level = undefined;
    tile_extra_height = 0;
    tile_extra_width  = 0;
    map_textures.length = 0;
    map_tiles.length = 0;
    map_animations.length = 0;
    map_layers.length = 0;
    map_objects.length = 0;
    map_enemies.length = 0;
    map_hints.length = 0;
    level_width = 0;
    level_height = 0;

    ins.length = 0;
    outs.length = 0;

    const _load = $.Deferred();
    const _xml  = $.Deferred();
    let level_assets_were_loaded = false;
    if (level_loaders[level_name]) {
        level_assets_were_loaded = true;
        _xml.resolve();
    }
    else {
        level_loaders[level_name] = new PIXI.loaders.Loader();
        $.get('level/' + level_name + '.tmx', function(data) {
            level_xmls[level_name] = $(data).children('map');
            _xml.resolve();
        }, 'xml');
    }

    _xml.done(function() {
        const $map = level_xmls[level_name];
        map_tile_width  = parseInt( $map.attr('tilewidth') );
        map_tile_height = parseInt( $map.attr('tileheight') );

        $map.children('tileset').each(function() {
            const $tileset = $(this);
            const firstgid   = parseInt( $tileset.attr('firstgid') );
            const tilewidth  = parseInt( $tileset.attr('tilewidth') );
            const tileheight = parseInt( $tileset.attr('tileheight') );
            const spacing    = parseInt( $tileset.attr('spacing') || 0 );
            const margin     = parseInt( $tileset.attr('margin') || 0 );
            const columns    = parseInt( $tileset.attr('columns') );
            const tilecount  = parseInt( $tileset.attr('tilecount') );

            const $image = $(this).children('image').first();
            if ($image.length) {
                const texture = $image.attr('source');
                map_textures.push(texture);
                for (let id = 0; id < tilecount; id++) {
                    const ty = Math.floor(id / columns);
                    const tx = id % columns;
                    map_tiles[firstgid + id] = {
                        't': map_textures.length - 1,
                        'x': margin + tx * (tilewidth + spacing),
                        'y': margin + ty * (tileheight + spacing),
                        'w': tilewidth,
                        'h': tileheight,
                    }
                }
            }
            $tileset.children('tile').each(function() {
                const id = parseInt( $(this).attr('id') );
                const ty = Math.floor(id / columns);
                const tx = id % columns;

                $(this).children('properties').children('property').each(function() {
                    if ($(this).attr('type') == 'int') {
                        map_tiles[firstgid + id][$(this).attr('name')] = parseInt( $(this).attr('value') );
                    }
                });

                const $image = $(this).children('image').first();
                if ($image.length) {
                    const texture = $image.attr('source');
                    const w = parseInt( $image.attr('width') );
                    const h = parseInt( $image.attr('height') );
                    if (tile_extra_height < h - map_tile_height) {
                        tile_extra_height = h - map_tile_height;
                    }
                    if (tile_extra_width < w - map_tile_width) {
                        tile_extra_width = w - map_tile_width;
                    }
                    map_textures.push(texture);
                    map_tiles[firstgid + id] = {
                        't': map_textures.length - 1,
                        'x': 0,
                        'y': 0,
                        'w': w || tilewidth,
                        'h': h || tileheight,
                    }
                }

                const $animation = $(this).children('animation').first();
                if ($animation.length) {
                    map_animations[firstgid] = [];
                    $animation.children('frame').each(function() {
                        map_animations[firstgid].push( parseInt(  $(this).attr('tileid') ) );
                    });
                }
            });
        });

        $map.children('layer').each(function() {
            const $layer = $(this);
            const width  = parseInt( $layer.attr('width') );
            const height = parseInt( $layer.attr('height') );
            if (level_width === undefined || level_width < width * map_tile_width) {
                level_width = width * map_tile_width;
            }
            if (level_height === undefined || level_height < height * map_tile_height) {
                level_height = height * map_tile_height;
            }

            const layer_properties = parse_properties($layer);

            const layer = {
                rows: [],
                parallax: layer_properties.parallax || 1,
                overlay:  layer_properties.overlay  || false,
            };
            map_layers.push(layer);

            const csv = $layer.children('data').text();
            const rows = csv.split(/\n/);
            for (const row of rows) {
                if (!row) {
                    continue;
                }

                const layer_row = [];
                layer.rows.push(layer_row);

                const cells = row.split(/,/);
                for (const cell of cells) {
                    if (!cell) {
                        continue;
                    }
                    layer_row.push(cell);
                }
            }
        });

        $map.children('objectgroup[name="images"]').each(function() {
            const $objectgroup = $(this);
            $objectgroup.children('object').each(function() {
                map_objects.push({
                    'tile': parseInt( $(this).attr('gid') ),
                    'x'   : parseInt( $(this).attr('x') ),
                    'y'   : parseInt( $(this).attr('y') ),
                });
            });
        });

        const map_body_def = new b2BodyDef();
        map_body = world.CreateBody(map_body_def);

        $map.children('objectgroup[name="walls"]').each(function() {
            const $objectgroup = $(this);
            $objectgroup.children('object').each(function() {
                const ox = parseInt( $(this).attr('x') );
                const oy = parseInt( $(this).attr('y') );
                if ( $(this).attr('width') && $(this).attr('height') ) {
                    const ex = ox + parseInt( $(this).attr('width') );
                    const ey = oy + parseInt( $(this).attr('height') );

                    create_wall(ox, oy, ex, oy);
                    create_wall(ex, oy, ex, ey);
                    create_wall(ex, ey, ox, ey);
                    create_wall(ox, ey, ox, oy);
                }
                $(this).children('polyline,polygon').each(function() {
                    const points = $(this).attr('points').split(' ');
                    let px;
                    let py;
                    for (const point of points) {
                        const [x, y] = point.split(',').map( c => parseInt(c) );
                        if (px != undefined && py != undefined) {
                            create_wall(ox + px, oy + py, ox + x, oy + y);
                        }
                        px = x;
                        py = y;
                    }

                    if ($(this).prop('tagName') == 'polygon') {
                        create_wall(ox + px, oy + py, ox, oy);
                    }
                });
            });
        });

        $map.children('objectgroup[name="gates"]').each(function() {
            const $objectgroup = $(this);
            $objectgroup.children('object').each(function() {
                const [type, id] = $(this).attr('name').split(':');
                if (type == 'in') {
                    ins.push({
                        'x': parseInt( $(this).attr('x') ),
                        'y': parseInt( $(this).attr('y') ),
                    });
                }
                if (type == 'out') {
                    const ox = parseInt( $(this).attr('x') );
                    const oy = parseInt( $(this).attr('y') );
                    const ex = ox + parseInt( $(this).attr('width') );
                    const ey = oy + parseInt( $(this).attr('height') );
                    outs.push({
                        'ox': ox,
                        'oy': oy,
                        'ex': ex,
                        'ey': ey,
                        'to': id,
                    });
                }
            });
        });

        $map.children('objectgroup[name="enemies"]').each(function() {
            const $objectgroup = $(this);
            $objectgroup.children('object').each(function() {
                const [type, id] = $(this).attr('name').split(':');
                map_enemies.push({
                    'x'     : parseInt( $(this).attr('x') ),
                    'y'     : parseInt( $(this).attr('y') ),
                    'type'  : type,
                });
            });
        });

        $map.children('objectgroup[name="hints"]').each(function() {
            const $objectgroup = $(this);
            $objectgroup.children('object').each(function() {
                const properties = parse_properties( $(this) );

                const ox = parseInt( $(this).attr('x') );
                const oy = parseInt( $(this).attr('y') );
                if ( $(this).attr('width') && $(this).attr('height') ) {
                    const ex = ox + parseInt( $(this).attr('width') );
                    const ey = oy + parseInt( $(this).attr('height') );

                    map_hints.push({
                        'ox': ox,
                        'oy': oy,
                        'ex': ex,
                        'ey': ey,
                        'text': properties.text || '',
                    });
                }
            });
        });


        if (level_assets_were_loaded) {
            _load.resolve();
        }
        else {
            for (let texture of map_textures) {
                level_loaders[level_name].add(texture, 'level/' + texture);
            }

            level_loaders[level_name].load( (loader, resources) => {
                _load.resolve();
            } );
        }
    });

    _load.done(function() {
        current_level = level_name;
    });

    return _load;
}

function parse_properties($root) {
    const properties = {}
    $root.children('properties').children('property').each(function() {
        if ($(this).attr('type') == 'int') {
            properties[$(this).attr('name')] = parseInt( $(this).attr('value') );
        }
        else if ($(this).attr('type') == 'float') {
            properties[$(this).attr('name')] = parseFloat( $(this).attr('value') );
        }
        else if ($(this).attr('type') == 'bool') {
            properties[$(this).attr('name')] = $(this).attr('value') == 'true' ? true : false;
        }
        else {
            properties[$(this).attr('name')] = $(this).attr('value') || $(this).text();
        }
    });

    return properties;
}

function create_wall(x1, y1, x2, y2) {
    let edge_shape = new b2EdgeShape();
    edge_shape.Set( new b2Vec2(x1 / world_scale, y1 / world_scale), new b2Vec2(x2 / world_scale, y2 / world_scale) )
    let fixture_def = new b2FixtureDef();
    fixture_def.set_shape(edge_shape);
    map_body.CreateFixture(fixture_def);
}

const TILE_TEXTURE_SIZE = 1024;
let   tile_containers;
let   parallax_containers;

function get_container_for_tile(layer, x, y) {
    const tx = Math.floor(x / TILE_TEXTURE_SIZE);
    const ty = Math.floor(y / TILE_TEXTURE_SIZE);

    // const parallax = map_layers[layer].parallax; // probably won't need it in this project
    const parallax = layer;
    const parallax_containers = get_parallax_container(layer);
    if (!tile_containers[parallax][tx + '_' + ty]) {
        tile_containers[parallax][tx + '_' + ty] = new PIXI.Container();
    }

    return tile_containers[parallax][tx + '_' + ty];
}
function get_parallax_container(layer) {
    const parallax = layer;
    if (!tile_containers[parallax]) {
        tile_containers[parallax] = {};
        parallax_containers[parallax] = new PIXI.Container();
        if (map_layers[layer].overlay) {
            map_overlay_container.addChild(parallax_containers[parallax]);
        }
        else {
            map_container.addChild(parallax_containers[parallax]);
        }
    }
    return parallax_containers[parallax];
}

function draw_level() {
    for (const i in map_tiles) {
        map_tiles[i].texture = new PIXI.Texture(
            level_loaders[current_level].resources[ map_textures[map_tiles[i].t] ].texture,
            new PIXI.Rectangle(map_tiles[i].x, map_tiles[i].y, map_tiles[i].w, map_tiles[i].h),
        );
    }
    // console.log(map_tiles);

    tile_containers = {};
    parallax_containers = {};
    for (const l in map_layers) {
        for (const y in map_layers[l].rows) {
            for (const x in map_layers[l].rows[y]) {
                const cell = parseInt(map_layers[l].rows[y][x]);
                if (cell == 0) {
                    continue;
                }

                const tx = x * map_tile_width;
                const ty = y * map_tile_height;
                const th = map_tiles[ map_layers[l].rows[y][x] ].h;

                let tile_sprite;
                let container;
                if (map_animations[cell]) {
                    const textures = [];
                    for (let t of map_animations[cell]) {
                        textures.push(map_tiles[cell + t].texture);
                    }
                    tile_sprite = new PIXI.extras.AnimatedSprite(textures);
                    tile_sprite.animationSpeed = 0.12;
                    tile_sprite.play();
                    container = get_parallax_container(l);
                    tile_sprite.x = tx;
                    tile_sprite.y = ty;
                }
                else {
                    tile_sprite = new PIXI.Sprite(map_tiles[cell].texture);
                    container = get_container_for_tile(l, tx, ty);
                    tile_sprite.x = tx;
                    tile_sprite.y = ty + tile_extra_height;
                }
                container.addChild(tile_sprite);
                tile_sprite.anchor.x = 0;
                tile_sprite.anchor.y = 1 - (map_tile_height / th);
            }
        }
    }

    for (const parallax in tile_containers) {
        for (const pos in tile_containers[parallax]) {
            const [tx, ty] = pos.split('_');
            const x = tx * TILE_TEXTURE_SIZE;
            const y = ty * TILE_TEXTURE_SIZE;

            tile_containers[parallax][pos].x = -x;
            tile_containers[parallax][pos].y = -y;

            const rt = PIXI.RenderTexture.create(TILE_TEXTURE_SIZE + tile_extra_width, TILE_TEXTURE_SIZE + tile_extra_height);
            app.renderer.render(tile_containers[parallax][pos], rt);

            const sprite = new PIXI.Sprite(rt);
            sprite.x = x;
            sprite.y = y - tile_extra_height;
            parallax_containers[parallax].addChild(sprite);
        }
    }

    for (const object of map_objects) {
        const tile_sprite = new PIXI.Sprite(map_tiles[object.tile].texture);
        props_container.addChild(tile_sprite);

        let z_index = object.y;
        if (map_tiles[object.tile].anchor_y) {
            z_index += map_tiles[object.tile].anchor_y - tile_sprite.height;
        }
        // tile_sprite.zIndex = z_index;
        tile_sprite.x = object.x;
        tile_sprite.y = object.y;
        tile_sprite.anchor.x = 0;
        tile_sprite.anchor.y = 1;
    }
}


