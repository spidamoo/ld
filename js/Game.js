
let current_level;
let last_level;
let player;
let hint_text;
let hint_text_ttl = 1.5;
let win = false;
let win_timer;
let gameover_timeout;
const game_scale = 1;
const characters = [];
const particles = [];

let global_container;
let map_container;
let map_overlay_container;
let objects_on_map_layer;
let character_layer;
let props_container;
let effects_container;

let viewport_width;
let viewport_height;


let B2D;
let world;
let world_scale = 100;
let ZERO_VECTOR;

function init_game() {
    const _init = $.Deferred();

    global_container        = new PIXI.Container();
    map_container           = new PIXI.Container();
    map_overlay_container   = new PIXI.Container();
    objects_on_map_layer    = new PIXI.Container();

    character_layer         = new PIXI.Container();
    props_container         = new PIXI.Container();
    effects_container       = new PIXI.Container();

    init_particle_containers();

    global_container.scale.x = game_scale;
    global_container.scale.y = game_scale;
    app.stage.addChild(global_container);

    global_container.addChild(map_container);
    global_container.addChild(objects_on_map_layer);
    global_container.addChild(map_overlay_container);

    objects_on_map_layer.addChild(props_container);
    objects_on_map_layer.addChild(character_layer);
    objects_on_map_layer.addChild(effects_container);

    hint_text = new PIXI.Text(
        '',
        new PIXI.TextStyle({
            fontFamily: 'monowidth',
            fontSize: 40,
            fill: ['#dddddd', '#ffffff', '#dddddd'], // gradient
            stroke: '#111111',
            strokeThickness: 5,
            align: 'center',
        })
    );
    hint_text.anchor.x = 0.5;
    hint_text.anchor.y = 1;
    hint_text.visible = false;
    app.stage.addChild(hint_text);

    if (DEBUG_DRAW) {
        objects_on_map_layer.addChild( getDebugDrawDisplayObject() );
    }

    const _init_box2d = init_box2d();

    _init_box2d.done(function() {
        _init.resolve();
    });

    return _init;
}

function init_box2d() {
    const _init = $.Deferred();

    Box2D().then(function(b2d) {
        B2D = b2d;
        using(B2D);

         ZERO_VECTOR = new b2Vec2(0, 0);

        _init.resolve();
    });

    return _init;
}

function init_world() {
    const gravity = new B2D.b2Vec2(0.0, 0.1);
    world = new B2D.b2World(gravity);

    if (DEBUG_DRAW) {
        world.SetDebugDraw( getDebugDraw() );
    }

    const listener = new JSContactListener();
    listener.BeginContact = function (contactPtr) {
        const contact = wrapPointer( contactPtr, b2Contact );
        const fixtureA = contact.GetFixtureA();
        const fixtureB = contact.GetFixtureB();
        const bodyA = fixtureA.GetBody();
        const bodyB = fixtureB.GetBody();

        const octopus_body  = (bodyA.octopus  && bodyA) || (bodyB.octopus  && bodyB);
        const enemy_body    = (bodyA.enemy    && bodyA) || (bodyB.enemy    && bodyB);
        const tentacle_body = (bodyA.tentacle && bodyA) || (bodyB.tentacle && bodyB);

        if (enemy_body && tentacle_body) {
            enemy_body.enemy.hit_with_tentacle(tentacle_body);
        }
        if (octopus_body && enemy_body) {
            player.hit_by_enemy(enemy_body);
        }
        // console.log(contact, fixtureA.GetBody(), fixtureB.GetBody());
    }

    // Empty implementations for unused methods.
    listener.EndContact = function() {};
    listener.PreSolve = function() {};
    listener.PostSolve = function() {};

    world.SetContactListener( listener );
}

function update_game(dt) {
    if (!current_level) {
        return;
    }

    world.Step(dt, 6, 2);
    if (DEBUG_DRAW) {
        clearDebugDraw();
        world.DrawDebugData();
    }

    objects_on_map_layer.x = viewport_width  * 0.5 - player.x;
    objects_on_map_layer.y = viewport_height * 0.5 - player.y;

    if (objects_on_map_layer.x > 0) {
        objects_on_map_layer.x = 0;
    }
    else if ( objects_on_map_layer.x - app.renderer.width < - level_width * game_scale ) {
        objects_on_map_layer.x = - level_width * game_scale + app.renderer.width;
    }

    if (objects_on_map_layer.y > 0) {
        objects_on_map_layer.y = 0;
    }
    else if ( objects_on_map_layer.y - app.renderer.height < - level_height * game_scale ) {
        objects_on_map_layer.y = - level_height * game_scale + app.renderer.height;
    }

    for (let parallax in parallax_containers) {
        parallax_containers[parallax].x = objects_on_map_layer.x; //  / parallax;
        parallax_containers[parallax].y = objects_on_map_layer.y; //  / parallax;
    }

    player.update(dt);
    if (!gameover_timeout) {
        if (player.dead) {
            PIXI.loader.resources.game_over.sound.play();
            gameover_timeout = setTimeout(function() {
                game_over();
                gameover_timeout = undefined;
            }, 3000);
        }
        else if (player.won) {
            setTimeout(function() {
                PIXI.loader.resources.win.sound.play();
            }, 1000);
            gameover_timeout = setTimeout(function() {
                game_over(true);
                gameover_timeout = undefined;
            }, 3000);
        }
    }
    else {
        PIXI.loader.resources.music.sound.volume -= dt * 0.3333;
    }

    for (const char of characters) {
        char.update(dt);
    }
    for (const particle of particles) {
        if (particle === undefined) {
            continue;
        }
        particle.update(dt);
    }

    if (Math.random() < level_width * 0.00002) {
        add_particle('light', Math.random() * level_width, 0);
    }
    if (Math.random() < level_width * 0.00002) {
        add_particle('bubbles', Math.random() * level_width, level_height);
    }
}

function start_level(level) {
    init_world();

    player = new Octopus();

    load_level(level).done(function() {
        app.renderer.resize( Math.min(window_w, level_width), Math.min(window_h, level_height) );
        viewport_width  = app.renderer.width  / game_scale;
        viewport_height = app.renderer.height / game_scale;
        hint_text.x = app.renderer.width  * 0.5;
        hint_text.y = app.renderer.height;

        draw_level();

        for (let out of outs) {
            if (out.to == '<win>') {
                const cx = out.ox + (out.ex - out.ox) * 0.5;
                const cy = out.oy + (out.ey - out.oy) * 0.5;

                const altar = new Altar(cx, cy);
                altar.init();
                altar.draw();
                characters.push(altar);
                altar.put_at(cx, cy);
                console.log(altar);
            }
        }

        player.init();
        player.draw();

        for (let e of map_enemies) {
            const enemy = new enemy_classes[e.type](e.x, e.y);
            enemy.init();
            enemy.draw();
            characters.push(enemy);
            enemy.put_at(e.x, e.y);
        }

        if (ins[0]) {
            player.put_at(ins[0].x, ins[0].y);
        }
        else {
            player.put_at(0, 0);
        }

        PIXI.loader.resources.enter_level.sound.play();

        app.ticker.start();
    });
}

function finish_level() {
    app.ticker.stop();
    last_level = current_level;
    current_level = undefined;
    player.destroy();
    // world.Destroy();
    for (let char of characters) {
        char.destroy();
    }
    characters.length = 0;
    clear_particles();

    for (let parallax in parallax_containers) {
        parallax_containers[parallax].destroy();
    }
    for (let parallax in tile_containers) {
        for (let pos in tile_containers[parallax]) {
            tile_containers[parallax][pos].destroy();
        }
    }
    map_container.removeChildren();
    map_overlay_container.removeChildren();
    character_layer.removeChildren();
    props_container.removeChildren();
    effects_container.removeChildren();
}
