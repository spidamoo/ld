const particle_textures = {
    'inkspot': ['inkspot', 'inkspot2'],
};

class Particle {
    constructor(id, type, x, y) {
        this.id = id;
        this.type = type;
        this.x = x;
        this.y = y;

        switch (type) {
            case 'light':
                break;
            case 'spark':
                this.ttl = 0.2;
                break;
            case 'inkspot':
                this.ttl = 1;
                break;
            case 'bubbles':
                break;
        }

        this.draw();
    }

    draw() {

        const self = this;
        switch (this.type) {
            case 'light':
                this.graphics = new PIXI.extras.AnimatedSprite(
                    PIXI.loader.resources[this.type].spritesheet.animations[this.type]
                );
                this.graphics.animationSpeed = 0.1;
                this.graphics.loop = false;
                this.graphics.play();
                this.graphics.onComplete = function() {
                    self.destroy();
                }
                this.graphics.anchor.set(0.5, 0);
                break;
            case 'bubbles':
                this.graphics = new PIXI.extras.AnimatedSprite(
                    PIXI.loader.resources[this.type].spritesheet.animations[this.type]
                );
                this.graphics.animationSpeed = 0.1;
                this.graphics.play();
                this.graphics.anchor.set(0.5, 0);
                this.gravity = -1;
                this.dy = -100;
                break;
            case 'spark':
                const points = [];
                const points_num = Math.floor(Math.random() * 5) + 3;
                for (let i = 0; i < points_num + 1; i++) {
                    points.push(new PIXI.Point(100 * i / points_num, 0)); 
                }
                this.graphics = new PIXI.mesh.Rope(PIXI.loader.resources.medusalightning.texture, points);

                const dx = (player.x - this.x) / points_num;
                const dy = (player.y - this.y) / points_num;
                for (let i = 0; i < points_num; i++) {
                    points[i].x = (i * dx) + Math.random() * 30 - 15;
                    points[i].y = (i * dy) + Math.random() * 30 - 15;
                }
                points[points_num].x = player.x - this.x;
                points[points_num].y = player.y - this.y;

                break;
            case 'inkspot':
                const texture_name = particle_textures[this.type][
                    Math.floor( Math.random() * particle_textures[this.type].length )
                ];
                this.graphics = new PIXI.Sprite(PIXI.loader.resources[texture_name].texture);
                this.graphics.anchor.set(0.5, 0.5);
                this.x += Math.random() * 10 - 5;
                this.y += Math.random() * 10 - 5;
                this.r = Math.random() * Math.PI * 2;
        }

        this.graphics.visible = false;
    }

    update(dt) {
        if (this.gravity) {
            this.dy += this.gravity * dt;
        }
        if (this.dx !== undefined) {
            this.x += this.dx * dt;
        }
        if (this.dy !== undefined) {
            this.y += this.dy * dt;
        }
        if (this.dr !== undefined) {
            this.r += this.dr * dt;
            if (this.r > Math.PI) {
                this.r -= 2 * Math.PI;
            }
            else if (this.r < -Math.PI) {
                this.r += 2 * Math.PI;
            }
        }

        if (this.ttl !== undefined) {
            this.ttl -= dt;
            if (this.ttl < 0) {
                if (this.breakes_when_expires) {
                    this.break();
                }
                else {
                    this.destroy();
                }
                return;
            }
        }

        if (this.spawn_interval) {
            for ( let spawn of Object.keys(this.spawn_interval) ) {
                if (this.spawn_time[spawn] === undefined) {
                    this.spawn_time[spawn] = 0;
                }
                this.spawn_time[spawn] += dt;
                let x = this.x;
                let y = this.y;
                const step_x = (this.x - this.ox) * this.spawn_interval[spawn] / this.spawn_time[spawn];
                const step_y = (this.y - this.oy) * this.spawn_interval[spawn] / this.spawn_time[spawn];
                while (this.spawn_time[spawn] > this.spawn_interval[spawn]) {
                    add_particle(spawn, x, y, 0);
                    this.spawn_time[spawn] -= this.spawn_interval[spawn];
                    x -= step_x;
                    y -= step_y;
                }
            }
        }

        switch (this.type) {
            case 'inkspot': {
                this.graphics.alpha = this.ttl;
                this.graphics.scale.x = (1 - this.ttl);
                this.graphics.scale.y = (1 - this.ttl);
                this.r += dt;
            } break;
            case 'bubbles': {
                if (this.y < 0) {
                    this.destroy();
                }
            }
        }


        if (this.graphics) {
            this.graphics.x = this.x;
            this.graphics.y = this.y;
            if (this.r !== undefined) {
                this.graphics.rotation = this.r;
            }

            this.graphics.visible = true;
        }
    }

    break(source) {
        this.destroy();
    }
    destroy() {
        remove_particle(this.id);
    }
}

const containers = {};
const container_blend_modes = {'fire': PIXI.BLEND_MODES.ADD};
function init_particle_containers() {
    for (let contained_type of []) {
        containers[contained_type] = new PIXI.particles.ParticleContainer(10000, {
            // scale: true,
            // position: true,
            // rotation: true,
            // alpha: true,
            // uvs: true,
            scale: true,
            position: true,
            rotation: true,
            uvs: true,
            alpha: true,
            tint: true,
        })
        if (container_blend_modes[contained_type] !== undefined) {
            containers[contained_type].blendMode = container_blend_modes[contained_type];
        }
        effects_container.addChild( containers[contained_type] );
    }
}
let free_particle_ids = [];
function get_container_for_particle(particle) {
    if (containers[particle.type]) {
        return containers[type];
    }
    return effects_container;
}
function add_particle(type, x, y, r) {
    const new_id = free_particle_ids.length ? free_particle_ids.shift() : particles.length;
    particles[new_id] = new Particle(new_id, type, x, y, r);
    if (particles[new_id].graphics) {
        get_container_for_particle(particles[new_id]).addChild(particles[new_id].graphics);
    }
    return particles[new_id];
}
function remove_particle(id) {
    if (!particles[id]) {
        return;
    }
    free_particle_ids.push(id);
    if (particles[id].graphics) {
        get_container_for_particle(particles[id]).removeChild(particles[id].graphics);
    }
    particles[id] = undefined;
}

function clear_particles() {
    free_particle_ids.length = 0;
    particles.length = 0;
}
