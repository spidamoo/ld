<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="env" tilewidth="200" tileheight="80" tilecount="7" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <terraintypes>
  <terrain name="bottom" tile="0"/>
 </terraintypes>
 <tile id="5">
  <image width="35" height="35" source="../img/background/clam1.png"/>
 </tile>
 <tile id="6">
  <image width="35" height="35" source="../img/background/clam2.png"/>
 </tile>
 <tile id="7">
  <image width="195" height="57" source="../img/background/sand 1.png"/>
 </tile>
 <tile id="8">
  <image width="120" height="30" source="../img/background/sand2.png"/>
 </tile>
 <tile id="9">
  <image width="200" height="60" source="../img/background/sand3.png"/>
 </tile>
 <tile id="10">
  <image width="60" height="80" source="../img/background/stone1.png"/>
 </tile>
 <tile id="11">
  <image width="60" height="80" source="../img/background/stone2.png"/>
 </tile>
</tileset>
