<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="vawes" tilewidth="256" tileheight="80" tilecount="16" columns="8">
 <image source="../img/background/vawes.png" trans="fffdfe" width="2048" height="160"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="4" duration="200"/>
   <frame tileid="5" duration="200"/>
   <frame tileid="6" duration="200"/>
   <frame tileid="7" duration="200"/>
   <frame tileid="8" duration="200"/>
  </animation>
 </tile>
</tileset>
